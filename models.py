# This file is part of the Django SMS Office, which has been
# developed within theMoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from django.utils.timezone import now as tznow
from django.utils.translation import ugettext_lazy as _


# This class is based on the implementation made
# in the dj-twilio-sms app
# see https://github.com/mastizada/dj-twilio-sms/blob/master/dj_twilio_sms/models.py
class OutgoingSMS(models.Model):
    sms_sid = models.CharField(max_length=34, default="", null=True, blank=True)
    from_number = models.CharField(max_length=30, default="", null=True, blank=True)
    to_number = models.CharField(max_length=30)
    body = models.TextField(max_length=160, default="", blank=True)
    sent_at = models.DateTimeField(null=True, blank=True)
    statuses = [
        ("queued", "Queued"),
        ("sent", "Sent"),
    ]
    status = models.CharField(max_length=20, choices=statuses, default="queued")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Outgoing SMS")
        verbose_name_plural = _("Outgoing SMS")

    def save(self, *args, **kwargs):
        if self.status == "sent" and not self.sent_at:
            self.sent_at = tznow()
        super(OutgoingSMS, self).save(*args, **kwargs)
