# Django SMS Client

Gather messages from server and send them
using the `gammu` library.

The `gammu` library can be found in Debian-based
distribution packaged under the name `python3-gammu`.
Since his installation via PIP often fails, it's
omitted from the `requirements.txt`.
