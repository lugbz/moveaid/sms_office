#!/usr/bin/python3
# This file is part of the Django SMS Office, which has been
# developed within theMoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import clientsettings
import gammu
import json
import os
import requests


q = requests.get(
    os.path.join(clientsettings.SERVER_URL, "api/sms/outgoingsms"),
    headers={"Authorization": f"Token {clientsettings.TOKEN}"}
)
queue = [dict(a) for a in json.loads(q.text)]
if not queue:
    exit()

sm = gammu.StateMachine()
sm.ReadConfig()
sm.Init()

for message in queue:
    message = {
        "Text": message["body"],
        "SMSC": {"Location": 1},
        "Number": message["to_number"],
    }
    sid = sm.SendSMS(message)
    requests.put(
        os.path.join(clientsettings.SERVER_URL, "api/sms/outgoingsms", str(queue[0]["id"])),
        {"from_number": clientsettings.MYNUMBER, "sms_sid": sid, "status": "sent"},
        headers={"Authorization": f"Token {clientsettings.TOKEN}"}
    )
