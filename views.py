# This file is part of the Django SMS Office, which has been
# developed within theMoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions
from sms_office.models import OutgoingSMS
from sms_office.permissions import SMSClientOnly
from sms_office.serializers import OutgoingSMSSerializer


class OutgoingSMSList(mixins.ListModelMixin, generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated, SMSClientOnly]
    serializer_class = OutgoingSMSSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        return OutgoingSMS.objects.filter(status="queued")


class OutgoingSMSUpdate(mixins.UpdateModelMixin, generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated, SMSClientOnly]
    serializer_class = OutgoingSMSSerializer

    def get_queryset(self):
        return OutgoingSMS.objects.filter(status="queued")

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
