# This file is part of the Django SMS Office, which has been
# developed within theMoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase, Client
import random
from sms_office.models import OutgoingSMS


class SMSOfficeTestCase(TestCase):
    def setUp(self):
        self.SMSUSER_USERNAME = getattr(settings, "SMSOFFICE_API_USERNAME", "smsclient")
        self.SMSUSER = User.objects.create_user(self.SMSUSER_USERNAME)
        self.SMSUSER_FAKE = User.objects.create_user(f"{self.SMSUSER}0")
        self.TESTSMS = [
            OutgoingSMS.objects.create(to_number="1234567890", body="HELLO!"),
            OutgoingSMS.objects.create(to_number="0987654321", body="HELLO!"),
            OutgoingSMS.objects.create(to_number="0754139628", body="HELLO!", status="sent")
        ]

    def test_OutgoingSMS_exists(self):
        self.assertGreater(len(OutgoingSMS.objects.all()), 0, "There is no OutgoingSMS in the DB")

    def test_unauthenticated_requests_failure(self):
        c = Client()
        response = c.get('/api/sms/outgoingsms')
        self.assertEqual(
            response.status_code, 403,
            "API not raising access denied on unauthenticated request"
        )

    def test_wrong_user_access_denied(self):
        c = Client()
        c.force_login(self.SMSUSER_FAKE)
        response = c.get('/api/sms/outgoingsms')
        self.assertEqual(
            response.status_code, 403,
            "API not raising access denied on unauthenticated request"
        )

    def test_get_queued_SMS_list(self):
        c = Client()
        c.force_login(self.SMSUSER)
        response = c.get('/api/sms/outgoingsms')
        self.assertEqual(response.status_code, 200, f"API error on queued SMS list, got HTTP {response.status_code}")
        self.assertEqual(
            len(response.json()),
            len(OutgoingSMS.objects.filter(status="queued")),
            "API error on queued SMS list, expected {} elements but got {}".format(
                len(OutgoingSMS.objects.filter(status="queued")),
                len(response.json())
            )
        )

    def test_update_queued_SMS(self):
        c = Client()
        c.force_login(self.SMSUSER)
        from_number = str(random.randint(1111111111, 9999999999))
        sid = str(random.randint(1111, 9999))
        mods = {"from_number": from_number, "sms_sid": sid, "status": "sent"}
        response = c.put(
            f'/api/sms/outgoingsms/{self.TESTSMS[0].id}',
            mods,
            content_type='application/json'
        )
        answ = response.json()
        self.assertEqual(response.status_code, 200, f"API error on SMS update, got HTTP {response.status_code}")
        for i in mods.keys():
            self.assertEqual(
                answ[i],
                mods[i],
                f"API error on SMS update, field {i} has not been updated"
            )

    def test_sms_has_sent_time_after_save(self):
        sms = OutgoingSMS.objects.create(to_number="0871253496", body="HELLO!")
        self.assertIsNone(sms.sent_at, "New SMS already has sent_at value")
        sms.status = "sent"
        sms.save()
        self.assertIsNotNone(sms.sent_at, "SMS has no sent_at value after saving as sent")

    def test_update_sent_SMS(self):
        c = Client()
        c.force_login(self.SMSUSER)
        response = c.put(
            f'/api/sms/outgoingsms/{self.TESTSMS[2].id}',
            content_type='application/json'
        )
        self.assertEqual(
            response.status_code, 404,
            "API not answering with 404 after updating sent SMS"
        )
