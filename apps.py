from django.apps import AppConfig


class SmsOfficeConfig(AppConfig):
    name = 'sms_office'
