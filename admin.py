# This file is part of the Django SMS Office, which has been
# developed within theMoveAid project.
# Copyright (C) 2021 Marco Marinello <mmarinello@unibz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib import admin
from sms_office import models


def register(model, **kw):
    class MyModelAdmin(admin.ModelAdmin):
        pass
    for i in kw:
        setattr(MyModelAdmin, i, kw[i])
    admin.site.register(model, MyModelAdmin)


register(
    models.OutgoingSMS,
    search_fields=["to_number", "body", "from_number"],
    list_display=["to_number", "from_number", "status", "created_at"],
    list_filter=["status", "from_number"]
)
